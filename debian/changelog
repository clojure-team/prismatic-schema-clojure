prismatic-schema-clojure (1.2.0-6) unstable; urgency=medium

  * Team upload.

  [ Vladimir Petko ]
  * d/control: remove tools-nrepl-clojure dependency, see #1076189.

  [ Jérôme Charaoui ]
  * d/control: bump Standards-Version, no changes needed
  * d/patches: switch org.clojure dependency to 1.x
  * d/patches: add patch to remove tools.nrpel

 -- Jérôme Charaoui <jerome@riseup.net>  Sun, 01 Sep 2024 11:00:23 -0400

prismatic-schema-clojure (1.2.0-5) unstable; urgency=medium

  * Team upload.
  * improve clean target (Closes: #1046684)
  * d/watch: drop uversionmangle, fix version regexp

 -- Jérôme Charaoui <jerome@riseup.net>  Sun, 18 Feb 2024 22:09:12 -0500

prismatic-schema-clojure (1.2.0-4) unstable; urgency=medium

  * Team upload.
  * d/control: flag all test-only deps with nocheck
  * d/control: remove hard-coded Depends for binary pkg
  * d/tests: add missing test dep, streamline classpath
  * d/rules: honor nocheck flag in dh_auto_test
  * d/changelog: fix syntax error
  * add d/gbp.conf with team defaults

 -- Jérôme Charaoui <jerome@riseup.net>  Thu, 26 Jan 2023 20:57:23 -0500

prismatic-schema-clojure (1.2.0-3) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/control: New email for the Clojure Team.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 06 Jan 2023 16:41:02 -0500

prismatic-schema-clojure (1.2.0-2) unstable; urgency=medium

  * Team upload.
  * d/tests: allow-stderr. (Closes: #1027390)
  * d/control: Standards-Version update to 4.6.2. No changes.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 06 Jan 2023 16:36:18 -0500

prismatic-schema-clojure (1.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/watch: fix broken file.
  * d/rules: deprecate the old pristmatic-schema.jar.
  * d/*.poms: install the java lib using the .poms file instead of d/rules.
  * d/rules: generate the patched pom.xml via lein.
  * d/control: remove cljx, as it's not needed to build anymore.
  * d/patches: rebase 0001 and 0002 for new upstream version.
  * d/tests: refactor unittests.
  * d/control: update copyright year.
  * d/control: Standards-Version update to 4.6.0. No changes.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 10 Dec 2021 13:34:03 -0500

prismatic-schema-clojure (1.1.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/rules: simplify doc handling.
  * d/rules: remove code to manage upstream tags, we should be using d/watch.
  * d/rules: build using leiningen. (Closes: #974706)
  * d/pom.xml: update pom.xml with lein for 1.1.12.
  * d/patches/0002: don't build cljs files.
  * d/control: update Standards Version to 4.5.1. Add Rules-Requires-Root.
  * d/control: use dh13.
  * d/tests: add autopkgtests.
  * Change the jar name from prismatic-schema to schema, as upstream does.
  * Keep installing pristmatic-schema.jar, but add a NEWS entry asking people
    to move away from it.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 07 Dec 2020 10:38:52 -0500

prismatic-schema-clojure (1.1.9-1) unstable; urgency=medium

  * Team upload.
  * debian/rules: added code to manage upstream tags.
  * New upstream release.
  * Push the package to the Clojure team (update VCS, switch to git tags
    workflow, reworked git from scratch, fixed maintainer field).
  * Standards-Version: 4.5.0.
  * Ran wrap-and-sort -bastk.
  * Switch to debhelper-compat.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Nov 2020 11:54:08 +0100

prismatic-schema-clojure (1.1.6-1) unstable; urgency=medium

  * Initial release (Closes: #855732)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 04 Aug 2017 16:13:27 -0400
